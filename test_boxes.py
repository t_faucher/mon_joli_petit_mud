from tp2 import Box, BoxContentError, Thing, BoxTooFullError, liste_from_yaml, Salle, Utilisateur
import yaml
def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_contains():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "machin" not in b

def test_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b

def test_remove_inexistant():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b
    try:
        b.remove("machin")
        assert False # On n'atteint pas cette ligne car
                     # une exception est levée
    except BoxContentError:
        pass

def test_box_open_close():
    b = Box()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_box_action_look():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.open()
    look = b.action_look()
    assert look == "La boite contient: truc1, truc2."
    b.close()
    look = b.action_look()
    assert look == "La boite est fermée."

def test_box_capacity():
    b = Box(5)
    assert b.capacity() == 5
    b.set_capacity(6)
    assert b.capacity() == 6

def test_create_thing():
    t = Thing(3)
    assert t.volume() == 3

def test_has_room_for():
    b = Box(5)
    t3 = Thing(3)
    t2 = Thing(2)
    t4 = Thing(4)
    b.add(t3)
    assert b.has_room_for(t2)
    assert not b.has_room_for(t4)

def test_add_capacity():
    b = Box(5)
    t3 = Thing(3)
    t2 = Thing(2)
    t4 = Thing(4)
    b.action_add(t3)
    try:
        b.action_add(t4)
        assert False, "erreur, on ne doit pas pouvoir mettre cet objet"
    except BoxTooFullError:
        pass
    b.action_add(t2) # on peut ajouter t2 car l'ajout de t4 n'a pas été fait
                     # donc il reste de la place.


def test_thing_create_simple_v2():
    chose = Thing(3,"bidule")

def test_thing_create():
    chose = Thing(3,"chose")
    assert chose.name() == "chose"
    assert chose.has_name("chose")
    chose.set_name('truc')
    assert chose.has_name("truc")
    assert chose.volume() == 3

def test_box_yaml():
    file = open("boites.yml", "r")
    l = yaml.load(file)
    for desc in l:
        b = Box.from_yaml(desc)
        assert b.capacity() == desc["capacity"]
        assert b.is_open() == desc["is_open"]

def test_thing_yaml():
    file = open("things.yml", "r")
    l = yaml.load(file)
    for desc in l:
        t = Thing.from_yaml(desc)
        assert t.volume() == desc["volume"]
        assert t.name() == desc["name"]

def test_liste_data():
    file = open("data.yml", "r")
    l = yaml.load(file)
    liste = liste_from_yaml(l)
    for i in range(len(liste)):
        if l[i]["type"] == 0:
            assert liste[i].capacity() == l[i]["capacity"]
            assert liste[i].is_open() == l[i]["is_open"]
        else:
            assert liste[i].name() == l[i]["name"]
            assert liste[i].volume() == l[i]["volume"]



def test_save_salle():
    s1 = Salle("IUT")
    b1 = Box()
    t1 = Thing(2,"Epee")
    t2 = Thing(3,"Potion")
    b1.action_add(t1)
    b1.action_add(t2)
    s1.ajout_box(b1)
    s1.save_salle()
#Test UTILISATEUR

def test_utilisateur_create():
    uti1 = Utilisateur("Lenny","TomTom",1)

def test_cree_salle():
    uti1 = Utilisateur("Lenny","TomTom",1)
    uti1.cree_salle("Salle1")
    assert uti1.get_salle("Salle1").get_nom() == "Salle1"
    assert not uti1.get_salle("Salle2")

def test_ajout_box():
    uti1 = Utilisateur("Lenny","TomTom",1)
    uti1.cree_salle("Salle1")
    Box1 = Box()
    uti1.ajout_box(Box1,"Salle1")

def test_user_creer_boite():
    util = Utilisateur("Faucher","Thomas",3)
    b1 = util.creer_boite()
    b2 = Box()
    assert b1 == b2

def test_save_salle():
    util = Utilisateur("Lenny", "Tomtom",1)
    util.cree_salle("Salle1")
    t1 = Thing(4,"Epee" )
    box = Box()
    box.action_add(t1)
    util.ajout_box(box,"Salle1")
    util.get_salle("Salle1").save_salle()
    try :
        with open("save.yml"): pass
    except IOError:
        pass