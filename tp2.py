class BoxContentError(Exception):
    pass

class BoxTooFullError(Exception):
    pass

class Salle:
    def __init__(self, nom_salle) -> None:
        self.nom_salle = nom_salle
        self.liste_box = []
    def get_nom(self):
        return self.nom_salle
    def ajout_box(self, box):
        self.liste_box.append(box)
    def __repr__(self) -> str:
        chaine = ""
        ind = 1
        for box in self.liste_box:
            chaine += f"La box {ind} : {box} "
            ind += 1
        return f"Cette salle se nomme {self.nom_salle} et contient {len(self.liste_box)} box : {chaine}"
 
    def save_salle(self):
        file = open("save.json", "w")
        file2 = open("save.yml", "w")
        file.write("- nameSalle : " + self.get_nom() + "\n")
        file2.write("- nameSalle : " + self.get_nom() + "\n")
        for box in self.liste_box:
            file.write(box.save_box_yaml())
            file2.write(box.save_box_yaml())
        file.close()
        file2.close()

class Utilisateur:
    def __init__(self, nom, prenom, id):
        self.nom = nom
        self.prenom = prenom
        self.id = id
        self.liste_salle = []

    def __repr__(self):
        return f"cette personne s'appelle {self.prenom} {self.nom} et sont identifiant est {self.id}."

    def cree_salle(self, nom_salle):
        self.liste_salle.append(Salle(nom_salle))

    def creer_boite(self, cap=None):
        return Box(cap)

    def ajout_box(self, box, nom_salle):
        for salle in self.liste_salle:
            if salle.get_nom() == nom_salle:
                salle.ajout_box(box)
    def get_salle(self, nom_salle):
        for salle in self.liste_salle:
            if salle.get_nom() == nom_salle:
                return salle

    @staticmethod
    def from_data(data):
        nom = data.get("nom", None)
        prenom = data.get("prenom", None)
        id = data.get("id", None)
        return Utilisateur(prenom , nom,id)

class Thing:
    def __init__(self, capacite, name=None):
        self.capacite = capacite
        self.lenom = name
    def volume(self):
        return self.capacite
    def name(self):
        return self.lenom
    def set_name(self, nom):
        self.lenom = nom
    def __repr__(self):
         return f"Le nom de cette boite est {self.lenom}"
    def has_name(self, nom):
        return self.lenom == nom

    @staticmethod
    def from_yaml(data):
        taille = data.get("volume")
        name = data.get("name")
        return Thing(taille,name)
        
    def save_thing_yaml(objet):
        return "- type : 1 \n  name : " + objet.name()+ "\n  volume : "+str(objet.volume()) +"\n"

class Box:
    def __init__(self, cap=None):
        self._contents = []
        self._open = False
        self._capacity = cap

    def capacity(self):
        return self._capacity

    def set_capacity(self, cap):
        self._capacity = cap

    def is_open(self):
        return self._open

    def open(self):
        self._open = True

    def close(self):
        self._open = False

    def action_add(self, objet):
        if not self.has_room_for(objet):
            raise BoxTooFullError
        self.add(objet)

    def add(self, objet):
        if self._capacity is not None:
            self._capacity -= objet.volume()
        self._contents.append(objet)

    def __contains__(self, objet):
        return objet in self._contents

    def remove(self, objet):
        if objet not in self:
            raise BoxContentError
        self._contents.remove(objet)

    def has_room_for(self, objet):
        if self.capacity() is None:
            return True
        return self.capacity() >= objet.volume()

    def action_look(self):
        if self.is_open():
            enumeration_contenu = ", ".join(self._contents)
            return "La boite contient: " + enumeration_contenu + "."
        else:
            return "La boite est fermée."

    def __repr__(self):
        return self.action_look()

    def __eq__(self, objet: object) -> bool:
        return self._contents == objet._contents and self._capacity == objet._capacity and self._open == objet._open

    @staticmethod
    def from_yaml(data):
        capacite = data.get("capacity")
        is_open = data.get("is_open")
        b = Box(capacite)
        if is_open:
            b.open()

        return b
    
    def save_box_yaml(objet):
        res = "- type : 0 \n" +"  capacity : " + str(objet.capacity())+ "\n  is_open : "+str(objet.is_open()) + "\n"
        for thing in objet._contents:
            res += thing.save_thing_yaml()
        return res

def liste_from_yaml(liste_data):
    liste = []
    for data in liste_data:
        type_o = data.get("type")
        if type_o == 1:
            
            liste.append(Thing.from_yaml(data))
        else:
            liste.append(Box.from_yaml(data))
    return liste